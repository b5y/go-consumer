package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
)

// ConsumerDB is used to set structure for storing in DB
type ConsumerDB struct {
	gorm.Model
	Email         string
	FullName      string
	TimesReceived uint64
}

func main() {
	db, err := gorm.Open("sqlite3", "users.db")
	if err != nil {
		log.Error("Cannot open sqlite3 database")
	}
	defer db.Close()
	db.AutoMigrate(&ConsumerDB{})
}
