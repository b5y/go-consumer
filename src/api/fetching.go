package api

import (
	"io"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/b5y/go-consumer/src/config"
)

// PostData is used to send data to consumer
type PostData struct {
	Email    string `json:"Email"`
	FullName string `json:"FullName"`
}

// FetchingData is used to get data from producer application
func FetchingData() (*PostData, error) {
	client := &http.Client{Timeout: time.Duration(config.Config.TimeOut) * time.Second}
	data := new(PostData)
	var buf io.Reader
	req, err := http.NewRequest("POST", config.Config.URL, buf)
	if err != nil {
		log.Error("Cannot get data: ", err.Error())
		return data, err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil || resp.StatusCode >= 400 {
		log.Warnf("Incorrect response: ", err.Error())
		log.Info("Error message: ", resp.StatusCode)
		return data, err
	}
	defer resp.Body.Close()
	return data, nil
}
