package db

import (
	"log"
	"sync/atomic"

	"github.com/jinzhu/gorm"
	"gitlab.com/b5y/go-consumer/src/config"
)

type postCreateFunc func(*gorm.DB) error

func InitMain(postCreate postCreateFunc) *gorm.DB {
	if !atomic.CompareAndSwapInt32(&panicInit, 0, 1) {
		panic("Attempt to init main database twice!")
	}

	dbtype, connstring := config.Config.GetConnString()
	db, err := gorm.Open(dbtype, connstring)
	if err != nil {
		log.Panic("Can't open database connection: ", err)
	}
	mainDB = db

	if config.Config.DB.Log {
		mainDB = mainDB.LogMode(true)
		mainDB.SetLogger(gorm.Logger{mainDBLogger{}})
	}
	if err := postCreate(mainDB); err != nil {
		panic(err)
	}
	return mainDB
}
