package config

import (
	"io/ioutil"

	log "github.com/Sirupsen/logrus"

	"gopkg.in/yaml.v2"
)

// Config is a global variable for configuration
var Config *ConsumerConfig

// ConsumerConfig contains all options to run the application
type ConsumerConfig struct {
	URL     string `yaml:"url_path"`
	TimeOut uint   `yaml:"time_out"`
}

// YamlLoad function returns data from yaml file.
// It doesn't need any changes at all. All you need is to change Config structure
// above if format of your yaml file has changed.
//		fileName: input file's name
func (cfg *ConsumerConfig) YamlLoad(fileName string) error {
	yamlFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Warnf("Cannot load yaml file #%v ", err.Error())
		return err
	}
	return yaml.Unmarshal(yamlFile, cfg)
}

// InitConfig is the default method for app configuration
func InitConfig(fileName string) (*ConsumerConfig, error) {
	cfg := ConsumerConfig{}
	err := cfg.YamlLoad(fileName)
	if err != nil {
		log.Warnf("Cannot initialize yaml file #%v ", err.Error())
		return nil, err
	}
	return &cfg, nil
}
